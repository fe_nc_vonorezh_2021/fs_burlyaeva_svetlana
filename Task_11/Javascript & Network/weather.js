const cityInput = document.querySelector('#city-input')


const showWeather = async () => {

    fetch(`https://api.openweathermap.org/data/2.5/weather?q=${cityInput.value}&appid=11b0170256c69f9dfb240ee028cbddf9`)
        .then(resp => resp.json())
        .then(function (data) {
            console.log(data);
            if (data.cod != '200') {
                const thing = data.message[0].toUpperCase() + data.message.slice(1);
                document.querySelector('.error').textContent = thing;
            } else {
                document.querySelector('.error').textContent = '';
                document.querySelector('.packageName').textContent = data.name;
                document.querySelector('.price').innerHTML = Math.round(data.main.temp - 273) + '&deg';
                const bigLetter = data.weather[0]['description'][0].toUpperCase() + data.weather[0]['description'].slice(1);
                document.querySelector('.disclaimer').textContent = bigLetter;
                document.querySelector('.features li').innerHTML = `<img src="https://openweathermap.org/img/wn/${data.weather[0]['icon']}@2x.png">`;
            }
            //document.querySelector('.error').textContent = "Eror";
        })
    // .catch(err => {
    //     document.querySelector('.error').textContent = err;
    // });
}

