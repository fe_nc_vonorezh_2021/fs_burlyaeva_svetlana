import {Component} from '@angular/core';
import {AppService} from './animals/app.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less'],
})
export class AppComponent {
  title = 'angmodules';

  constructor(public appService: AppService) {
  }
}
