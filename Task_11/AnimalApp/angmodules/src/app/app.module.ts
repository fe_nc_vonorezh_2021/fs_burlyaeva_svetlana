import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {AnimalsModule} from './animals/animals.module';
import {AppComponent} from './app.component';
import {EditAnimalPageComponent} from './animals/edit-animal-page/edit-animal-page.component'
import {AppRoutingModule} from './app-routing.module';


@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AnimalsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
