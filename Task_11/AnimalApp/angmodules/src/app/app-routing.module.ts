import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AnimalsComponent} from './animals/animals.component';
import {EditAnimalPageComponent} from './animals/edit-animal-page/edit-animal-page.component'


const routes: Routes = [
  {path: "", component: AnimalsComponent},
  {path: "animal/:id", component: EditAnimalPageComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
