import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AnimalsComponent} from './animals.component';
import {ReactiveFormsModule} from '@angular/forms';
import {FormsModule, FormControl, FormGroup} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {EditAnimalPageComponent} from './edit-animal-page/edit-animal-page.component';
import {RouterModule} from "@angular/router"

@NgModule({
  declarations: [
    AnimalsComponent,
    EditAnimalPageComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RouterModule,
    HttpClientModule
  ],
  exports: [
    AnimalsComponent,
  ]
})
export class AnimalsModule {
}
