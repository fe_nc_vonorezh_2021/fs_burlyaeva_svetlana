import {Component, Input, OnInit} from '@angular/core';
import {Animals} from './animals';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AppService} from './app.service';

@Component({
  selector: 'app-animals',
  templateUrl: './animals.component.html',
  styleUrls: ['./animals.component.less']
})
export class AnimalsComponent implements OnInit {

  @Input() animals!: Animals
  isOpen = true
  moreInfo: string[] = []

  animalForm!: FormGroup
  animalModelObj: Animals = new Animals();
  animalsData!: any;

  constructor(public appService: AppService) {
  }

  onSubmit() {

    if (this.animalForm.valid) {
      this.animalModelObj.type = this.animalForm.value.type;
      this.animalModelObj.breed = this.animalForm.value.breed;
      this.animalModelObj.name = this.animalForm.value.name;
      this.animalModelObj.age = this.animalForm.value.age;
      this.animalModelObj.color = this.animalForm.value.color;
      this.animalModelObj.favActive = this.animalForm.value.favActive;

      this.appService.postAnimal(this.animalModelObj)
        .subscribe(res => {
            console.log(res);
            alert("Зверушка успешно добавлена")
            let ref = document.getElementById('cancel');
            ref?.click();
            this.animalForm.reset();
            this.getAllAnimals();
          },
          err => {
            alert("Ошибка, повторите попытку")
          })
    }
  }

  getAllAnimals(): void {
    this.appService.getAnimal(this.animalForm)
      .subscribe(res => {
        this.animalsData = res;
      });
  }

  deleteAnimal(row: any) {
    this.appService.deleteAnimal(row.id)
      .subscribe(res => {
        alert("Успешно удалено");
        this.getAllAnimals();
      })
  }

  ngOnInit(): void {
    this.animalForm = new FormGroup({
      name: new FormControl('', [Validators.required, Validators.minLength(1)]),
      type: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(16)]),
      color: new FormControl(''),
      breed: new FormControl('',),
      age: new FormControl('', [Validators.minLength(1), Validators.pattern('[0-9]*')]),
      favActive: new FormControl('')
    });
    this.getAllAnimals();
  }
}
