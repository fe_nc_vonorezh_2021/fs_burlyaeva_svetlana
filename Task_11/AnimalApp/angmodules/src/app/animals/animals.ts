export class Animals {
  id: number = 0;
  type: string = '';
  breed: string = '';
  name: string = '';
  age: number = 0;
  color: string = '';
  favActive: string = '';
}
