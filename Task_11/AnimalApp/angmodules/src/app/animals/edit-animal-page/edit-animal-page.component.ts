import {Component, OnInit, OnDestroy} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {Subscription, switchMap} from 'rxjs';
import {AppService} from '../app.service';
import {Animals} from '../animals';


@Component({
  selector: 'app-edit-animal-page',
  templateUrl: './edit-animal-page.component.html',
  styleUrls: ['./edit-animal-page.component.less']
})
export class EditAnimalPageComponent implements OnInit {

  animalForm!: FormGroup
  id!: number;
  animalModelObj: Animals = new Animals();

  private subscriptions: Subscription[] = [];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private appService: AppService
  ) {
  }

  ngOnInit(): void {
    this.subscriptions.push(
      this.route.params.pipe(
        switchMap((params: Params) => {
          this.id = params['id'];
          return this.appService.getAnimal(params['id'])
        })
      ).subscribe((animal: Animals) => {
        this.animalForm = new FormGroup({
          name: new FormControl(animal.type, [Validators.required, Validators.minLength(1)]),
          type: new FormControl(animal.name, [Validators.required, Validators.minLength(1), Validators.maxLength(16)]),
          color: new FormControl(animal.color),
          breed: new FormControl(animal.breed),
          age: new FormControl(animal.age, [Validators.minLength(1), Validators.pattern('[0-9]*')]),
          favActive: new FormControl(animal.favActive)
        });
      })
    )
  }

  onEdit(row: any) {
    this.animalForm.controls['name'].setValue(row.name);
    this.animalForm.controls['type'].setValue(row.type);
    this.animalForm.controls['breed'].setValue(row.breed);
    this.animalForm.controls['color'].setValue(row.color);
    this.animalForm.controls['age'].setValue(row.age);
    this.animalForm.controls['favActive'].setValue(row.favActive);
  }

  // onSubmit() :void{ //эта штука сильно не работает и я не представляю, как ее заставить
  //  if(this.animalForm.valid){
  //    const animal: Animals = {...this.animalForm.value, id: this.id}
  //    this.subscriptions.push(this.appService.updateAnimal(animal).subscribe())
  //    console.log("Успешно изменено")
  //    this.animalForm.reset();
  //  }
  // }
  onSubmit(): void { //эта штука сильно не работает и я не представляю, как ее заставить
    if (this.animalForm.valid) {
      this.animalModelObj.name = this.animalForm.value.name;
      this.animalModelObj.type = this.animalForm.value.type;
      this.animalModelObj.breed = this.animalForm.value.breed;
      this.animalModelObj.color = this.animalForm.value.color;
      this.animalModelObj.age = this.animalForm.value.age;
      this.animalModelObj.favActive = this.animalForm.value.favActive;
      this.appService.updateAnimal(this.animalModelObj, this.animalModelObj.id)
    }
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(subscriptions => subscriptions.unsubscribe())
  }
}
