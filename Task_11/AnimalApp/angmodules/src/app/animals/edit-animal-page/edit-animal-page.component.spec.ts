import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditAnimalPageComponent } from './edit-animal-page.component';

describe('EditAnimalPageComponent', () => {
  let component: EditAnimalPageComponent;
  let fixture: ComponentFixture<EditAnimalPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditAnimalPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditAnimalPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
