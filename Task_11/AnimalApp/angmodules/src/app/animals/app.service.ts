import {Injectable} from '@angular/core';
import {Animals} from '../animals/animals';
import {HttpClient, HttpClientModule} from '@angular/common/http'
import {map} from 'rxjs/operators'

@Injectable({
  providedIn: 'root'
})
export class AppService {

  constructor(private http: HttpClient) {
  }

  postAnimal(animal: any) {
    return this.http.post<any>("http://localhost:3000/posts", animal)
      .pipe(map((res: any) => {
        return res;
      }))
  }

  getAnimal(animal: any) {
    return this.http.get<any>("http://localhost:3000/posts")
      .pipe(map((res: any) => {
        return res;
      }))
  }

  deleteAnimal(id: number) {
    return this.http.delete<Animals>("http://localhost:3000/posts/" + id)
      .pipe(map((res: Animals) => {
        return res;
      }))
  }


  updateAnimal(animal: Animals, id: number) {
    return this.http.put<Animals>("http://localhost:3000/posts/" + id, animal)
      .pipe(map((res: any) => {
        return res;
      }))
  }

}
