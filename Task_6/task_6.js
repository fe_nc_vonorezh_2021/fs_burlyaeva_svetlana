const mutationObserver = new MutationObserver((mutations) => {
    if (document.getElementById("formGroup")) {
        setupListeners();
        mutationObserver.disconnect();
    }
});

const form = document.getElementById('form');
const username = document.getElementById('firstName');
const userSurname = document.getElementById('lastName');
const email = document.getElementById('email');
const phone = document.getElementById('number');
const textArea = document.getElementById('textArea');

// const errorName = document.getElementById('error');
// const errorSurname = document.getElementById('error1');
// const errorEmail = document.getElementById('error2');
// const errorPhone = document.getElementById('error3');
// const errorTextArea = document.getElementById('error4');

form.addEventListener('submit', e => {
    e.preventDefault();

    checkInputs();
});

function checkInputs() {
    // trim to remove the whitespaces
    const usernameValue = username.value.trim();
    const userSurnameValue = userSurname.value.trim();
    const emailValue = email.value.trim();
    const phoneValue = phone.value.trim();
    const textAreaValue = textArea.value.trim();

    if (usernameValue === '') {
        setErrorFor(username, 'First name cannot be blank');
    } else if (!isName(usernameValue)) {
        setErrorFor(username, 'Not a valid first name');
    } else {
        setSuccessFor(username);
    }

    if (userSurnameValue === '') {
        setErrorFor(userSurname, 'Last name cannot be blank');
    } else if (!isName(userSurnameValue)) {
        setErrorFor(userSurname, 'Not a valid last name');
    } else {
        setSuccessFor(userSurname);
    }

    if (emailValue === '') {
        setErrorFor(email, 'Email cannot be blank');
    } else if (!isEmail(emailValue)) {
        setErrorFor(email, 'Not a valid email');
    } else {
        setSuccessFor(email);
    }

    if (phoneValue === '' || phoneValue == null) {
        setErrorFor(phone, 'Phone number cannot be blank');
    } else if (!isPhoneNumber(phoneValue)) {
        setErrorFor(phone, 'Not a valid phone number');
    } else {
        setSuccessFor(phone);
    }

    if (textAreaValue === '' || textAreaValue == null) {
        setErrorFor(textArea, 'Your appeal cannot be blank');
    } else {
        setSuccessFor(textArea);
    }
}

function setErrorFor(input, message) {
    const formGroup = input.parentElement;
    const small = formGroup.querySelector('small');
    formGroup.className = 'formGroup error';
    // errorName.innerText = message;
    // errorSurname.innerText = message;
    // errorEmail.innerText = message;
    // errorPhone.innerText = message;
    // errorTextArea.innerText = message;
    small.innerText = message;
}

function setSuccessFor(input) {
    const formGroup = input.parentElement;
    formGroup.className = 'formGroup success';
}

function isName(name) {
    return /[A-Za-zА-Яа-яЁё]/.test(userSurname);
}

function isEmail(email) {
    return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email);
}

function isPhoneNumber(phone) {
    return /^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/.test(phone);
}
