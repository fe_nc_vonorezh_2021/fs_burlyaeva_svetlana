const gulp = require('gulp');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');
const path = require('path')
const srcPath = path.resolve(__dirname, '../Task 4/**/*.js')
gulp.task('js', function () {
    return gulp.src(srcPath)
        /*Функция gulp.src() берёт один или несколько файлов или массив и возвращает поток,
         который может быть передан в плагины.*/
        .pipe(concat('test.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('./dist/'))
});
