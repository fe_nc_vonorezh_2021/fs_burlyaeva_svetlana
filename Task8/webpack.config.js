const path = require('path')

module.exports = {
    entry: './dist/test.min.js',
    //entry указывает webpack’y, какой из JavaScript файлов является основным
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'bundle.js'
    }
}
