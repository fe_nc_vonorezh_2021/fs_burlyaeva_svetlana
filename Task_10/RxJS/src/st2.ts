import {Observable, map, range, filter} from 'rxjs';

const stream$ = new Observable(object => {
    for (let i = 5; i > 0; i -= 1) {
        object.next(i)
        if (i == 1) {
           throw new Error('End');
            break;
        }
    }

})

stream$.subscribe(
    value => console.log(value),
    error => console.error(error)
)
