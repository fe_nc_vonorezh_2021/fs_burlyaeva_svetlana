import {range, filter} from "rxjs";
import {Observable} from "rxjs";


function simpleNum (n: number) {
    if (n === 1 || n === 0) {
        return false;
    } else {
        for(let i = 2; i < n; i++) {
            if(n % i === 0) {
                return false;
            }
        }
        return true;
    }
}
// function simpleNum(n: number) {
//     for (let i = 2; i <= n; i++) {
//         let flag = 1;
//         if (i > 2 && i % 2 != 0) {
//             for (let j = 3; j * j <= i; j = j + 2) {
//                 if (i % j == 0) {
//                     flag = 0;
//                 }
//             }
//         } else if (i != 2) flag = 0;
//         if (flag == 1) {
//             return i;
//         }
//     }
// }
    const stream$: Observable<number> = range(0, 100)
        .pipe(
            filter((n: number) => simpleNum(n))
        );
    stream$.subscribe(
        (n: number) => {
            console.log(n)
        }
    );
