import { fromEvent, merge } from "rxjs"

const body: HTMLElement | null = document.querySelector('body')
const getRandColor = (): string => {
    let first: number = Math.floor(Math.random() * 255);
    let second: number = Math.floor(Math.random() * 255);
    let third: number = Math.floor(Math.random() * 255);
    let str: string = 'rgb(' + first + ', ' + second + ', ' + third + ')';
    return str;
};

const f: any = document.querySelector('.first-button');
const s: any = document.querySelector('.second-button');
const t: any = document.querySelector('.third-button');
const firstStream$ = fromEvent(f, 'click');
const secondStream$ = fromEvent(s, 'click');
const thirdStream$ = fromEvent(t, 'click');

if (body != null) {
    merge(firstStream$, secondStream$, thirdStream$).subscribe(value => body.style.background = getRandColor());
    }