const path = require('path')

module.exports = {
    context: path.resolve(__dirname, 'src'),
    entry: {
        subtask1: './st1.js',
        subtask2: './st2.js',
        subtask3: './st3.js'
    },

    output: {
        filename: '[name].js',
        path: path.resolve(__dirname, 'dist')
    }
};
