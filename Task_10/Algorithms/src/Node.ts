export class N0de<T> {
    /*я хотела назвать класс нормально, но по какой-то причине
     появлялась ошибка, если я называла его Node, поэтому так*/
    data: T;
    next: N0de<T> | null;
    previous: N0de<T> | null;

    constructor(data: T) {
        this.data =  data;
        this.next =  null;
        this.previous = null;
    }
}
