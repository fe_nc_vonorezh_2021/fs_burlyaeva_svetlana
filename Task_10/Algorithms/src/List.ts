import {N0de} from "./Node";

export class LinkedList<T> {
    private head: N0de<T> | null;
    tail: N0de<T> | null;
    size: number;

    constructor() {
        this.head = null;
        this.tail = null;
        this.size = 0;
    }

    getElementByIndex(index: number) {
        if (!this.head) {
            console.log('Список пустой');
            return null;
        } else if (index < 0 || index >= this.size) {
            console.log('Вы вышли за границы списка');
            return null;
        } else {
            let curr: N0de<T> | any = this.head;
            let i: number = 0;
            while (i++ < index) {
                curr = curr.next;
            }
            return curr.data;

        }
    }

    public getList() {
        let arr: T[] = [];
        for (let index: number = 0; index < this.size; index++) {
            arr.push(this.getElementByIndex(index));
        }
        return arr;
    };

    addToBegin(data: T) {
        const node = new N0de(data);
        if (this.size == 0) {
            this.tail = this.head = node;
        } else if (this.head) {
            this.head.previous = node;
            node.next = this.head;
            this.head = node;
        }
        this.size++;
    }

    addToEnd(data: T) {
        const node = new N0de(data);
        if (this.size == 0) {
            this.tail = this.head = node
        } else if (this.tail) {
            this.tail.next = node;
            node.previous = this.tail;
            this.tail = node;
        }
        this.size++;
    }

    addByIndex(index: number, data: T) {
        const node = new N0de(data);
        if (index <= 1 || index >= this.size) {
            console.log('Вы вышли за границы списка');
            return null;
        } else {
            if (this.size == 0) {
                this.tail = this.head = node;
            } else if (index === this.size && this.tail != null) {
                node.previous = this.tail;
                this.tail.next = node;
                this.tail = node;
            } else {
                let curr: N0de<T> | any = this.head;

                let i = 0;
                while (++i < index) {
                    curr = curr.next;
                }
                node.next = curr;
                node.previous = curr.previous;
                curr.previous.next = node;
                curr.previous = node;
            }
            this.size++;
        }
    }

   /* editt(index: number, data: T) {
         if (index <= 1 || index >= this.size) {
             console.log("Вы вышли за границы списка")
         } else
             this.getElementByIndex(index).data = data;
     }*/

    public edit(index: number, data: T) {
        if (index < 0 || index > this.size) {
            console.log("Вы вышли за границы списка");
        } else {
            let i: number = 0;
            let curr: N0de<T> | any = this.head;
            while (i++ < index) {
                curr = curr.next;
            }
            curr.data = data;
        }
    }

    removeByData(data: T) {
        if (!this.head) {
            return console.log('Cписок пустой');
        }

        let current = this.head;

        if (current.data === data && current.next != null) {
            const next = current.next;
            next.previous = null;
            this.head = next;
        } else if (this.tail != null && this.tail.previous != null && this.tail.data === data) {
            const prev = this.tail.previous;
            prev.next = null;
            this.tail = prev;
        } else {
            while (current.next) {
                current = current.next;

                if (current.data === data && current.next != null && current.previous != null) {
                    const next = current.next;
                    const prev = current.previous;
                    next.previous = prev;
                    prev.next = next;
                }
            }
        }
        this.size--;
    }


}
