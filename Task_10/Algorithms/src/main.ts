import {LinkedList} from "./List"

const list: LinkedList <number> = new LinkedList();


list.addToBegin(1)
list.addToEnd(2)
list.addToEnd(3)
list.addToEnd(4)
list.addToEnd(5)
list.addToEnd(6)
list.addToEnd(7)
list.addToBegin(0)
list.addToEnd(9)
list.addToEnd(99)
list.addByIndex(9, 8)
list.removeByData(99)
list.addByIndex(1, 1)
list.edit(2,66)
console.log(list.getElementByIndex(7))
//console.log(list.getElementByIndex(5));
console.log(list.getList());
