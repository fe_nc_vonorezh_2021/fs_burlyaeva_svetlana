function quickSort(arr: number [], left: number, right: number) {
    let i: number = left;
    let j: number = right;
    let pivot = arr[Math.floor((left + right) / 2)];
/*вообще сколько помню в сортировках ее на лабах в универе использовали постоянно и мне кажется так принято.
 Возмножно нам объясняли на лекциях, но я такого не помню. Я всегда это понимала так:
 pivot можно перевести как точка опоры с английского, или точка вращения.
 И центр массива так назвали, потому что это значение с которым сравнивают все другие элементы массива.
 */
  //  while (i <= j) {
    /*по идее этот while проверяет меньше ли элемент из левой части массива элемента справа,
     но оказалось, он не нужен
     */
        while (arr[i] < pivot) {
            i++;
        }
        while (arr[j] > pivot) {
            j--;
        }
        if (i <= j) {
            [arr[i], arr[j]] = [arr[j], arr[i]];
            i++;
            j--;

        }
   // }
    if (left < j)
        quickSort(arr, left, j);
    if (i < right)
        quickSort(arr, i, right);
    return arr;
}


const n: number = 10;
let arr: number[] = [];
for (let i = 0; i < n; i++) {
    arr[i] = (Math.floor(Math.random() * 100) + 1);
}

console.log(arr);
console.log(quickSort(arr, 0, n - 1));

// let arr: number[] = [2, 5, 1, 4,0,9];
//
//
//
// console.log(arr);
// const result = quickSort(arr, 0, 6 - 1);
// console.log(result);

