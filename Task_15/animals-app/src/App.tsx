import React, {useState} from 'react';
import './App.css';
import AddForm from './components/AddForm/AddForm';
import CustomButton from './components/CustomButton/CustomButton'
import AnimalTable from './components/AnimalTable/AnimalTable'
import {ThemeProvider} from 'styled-components';
import styled from "styled-components";

const LightTheme = {
    pageBackground: "white",
    titleColor: "#dc658b",
    tagLineColor: "black"
};

const DarkTheme = {
    pageBackground: "#282c36",
    titleColor: "lightpink",
    tagLineColor: "lavender"
}

const themes: any = {
    light: LightTheme,
    dark: DarkTheme,
}

const initialValues = {
    id: '',
    type: '',
    breed: '',
    name: '',
    age: '',
    favActive: '',
};

const Page = styled.div`
   height: 100vh;
   width: 100%;
  background-color: ${props => props.theme.pageBackground};
`;

function AppAnimal() {

    const [animalData, setAnimalData] = useState(initialValues);
    const isFilledFields = animalData.type && animalData.name && animalData.age;
    const [animals, setAnimals]: any = useState([
        {id: 1, type: 'Улитка', breed: 'Ахатина', name: 'Ваня', age: 2, favActive: 'Ползать в бар'},
        {id: 2, type: 'Кот', breed: 'Мейн-кун', name: 'Фейс', age: 1, favActive: 'ЪуЪ'},
        {id: 3, type: 'Волк', breed: 'Тасманский', name: 'Павел', age: 8, favActive: 'Безумно быть первым'},
        {id: 4, type: 'Кот', breed: 'Поп', name: 'Овсянка', age: 2, favActive: 'Широко открывать пасть'}])
    const [withCats, setWithCats] = useState<boolean>(false);

    const handleSubmitAnimal = (e: any) => {
        e.preventDefault();
        if (isFilledFields) {
            setAnimals((prevState: any) => ([...prevState, animalData]))
            setAnimalData(initialValues);
        } else alert("Заполните обязательные поля (*)")
    }

    const hideCats = () => {
        (withCats)
            ? setAnimals(animals)
            : setAnimals((animals: any) => animals.filter((animal: any) => animal.type !== 'Кот'))
        setAnimals((animals: any) => animals)
        setWithCats(withCats => !withCats)
    }

    const handleCleanClick = (e: any) => {
        e.preventDefault();
        setAnimalData(initialValues);
    };

    const handleInputChange = (e: any, type: any) => setAnimalData((prevState) => ({
        ...prevState,
        [type]: e.target.value
    }))
    //при каждом введенном символе будет получать евент из которого будем доставать таргетювелью и записывать в нужный ключ
    const [theme, setTheme] = useState("light")

    function changeTheme() {
        if (theme === "light") {
            setTheme("dark");
        } else {
            setTheme("light");
        }
    };

    console.log(animalData);
    console.log(animals);


    return (
        <ThemeProvider theme={themes[theme]}>
            <Page>
                <div className="theme-button">
                    <CustomButton
                        label="Смени тему"
                        type="button"
                        handleClick={changeTheme}
                        className=""
                    />
                </div>
                <div className="wrapper">
                    <div className="wrapper-content">
                        <div className="table-data">
                            <AnimalTable
                                animals={animals}
                            />
                            <CustomButton
                                label="Спрячь котиков"
                                classNames="hide-cat"
                                handleClick={hideCats}
                            />
                        </div>
                        <div>

                            <form>
                                <h3 className='form-title'>Добавь зверушку</h3>
                                <AddForm
                                    placeholder="Вид*"
                                    handleChange={handleInputChange}
                                    value={animalData.type}
                                    fieldName="type"
                                />
                                <AddForm
                                    placeholder="Имя*"
                                    handleChange={handleInputChange}
                                    value={animalData.name}
                                    fieldName="name"
                                />
                                <AddForm
                                    placeholder="Порода"
                                    handleChange={handleInputChange}
                                    value={animalData.breed}
                                    fieldName="breed"
                                />
                                <AddForm
                                    placeholder="Возраст*"
                                    handleChange={handleInputChange}
                                    value={animalData.age}
                                    fieldName="age"
                                />
                                <AddForm
                                    placeholder="Любимое занятие"
                                    handleChange={handleInputChange}
                                    value={animalData.favActive}
                                    fieldName="favActive"
                                />

                                <div className="rs-buttons">
                                    <CustomButton
                                        label="Очистить"
                                        type="reset"
                                        handleClick={handleCleanClick}
                                        className=""

                                    /> <CustomButton
                                    label="Добавить"
                                    type="submit"
                                    disabled={!isFilledFields}
                                    handleClick={handleSubmitAnimal}
                                    className=""

                                />
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </Page>
        </ThemeProvider>


    )

}

export default AppAnimal;