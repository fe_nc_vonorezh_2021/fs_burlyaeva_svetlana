import React, {useState} from 'react';
import './AddForm.css'

const AddForm = (
    {
        placeholder,
        handleChange,
        value,
        fieldName
    }: any
) => {
    return (
        <input placeholder={placeholder}
               onChange={(e) => handleChange(e, fieldName)}
               value={value}
        />
    )
}

export default AddForm;