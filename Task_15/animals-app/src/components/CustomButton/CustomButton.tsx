import React, {useState} from 'react';
import './CustomButton.css';

const CustomButton = ({
                          label,
                          type = "button",
                          disabled = false,
                          handleClick,
                          classNames


                      }: any) => {
    return (
        <button
            className={classNames}
            onClick={handleClick}>
            {label}
        </button>
    )
}

export default CustomButton;