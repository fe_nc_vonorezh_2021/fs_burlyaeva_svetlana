import React, {useState} from 'react';
import './AnimalTable.css';

const AnimalTable = ({
                         animals
                     }: any) => {
    return (
        <table>
            <tr>
                <th>№</th>
                <th>Зверушка</th>
                <th>Вид</th>
                <th>Порода</th>
                <th>Имя</th>
                <th>Возраст</th>
                <th>Любимое занятие</th>
            </tr>

            <tbody>
            {animals.map((animal: any, id: number) => (
                <tr>
                    <td>{id + 1}</td>
                    <td>{animal.type} {animal.name}</td>
                    <td>{animal.type}</td>
                    <td>{animal.breed}</td>
                    <td>{animal.name}</td>
                    <td>{animal.age}</td>
                    <td>{animal.favActive}</td>
                </tr>
            ))}
            </tbody>
        </table>
    )
}

export default AnimalTable;