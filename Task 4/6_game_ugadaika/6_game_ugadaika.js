function gess() {
    let required_number = Math.round(Math.random() * (1000 - 1) + 1); //заданое
    let hidden_number = prompt("Введите число: ");  //которое вводится
    let count = 1;

    while (hidden_number != required_number) {

        count++;
        if (hidden_number.length == '' || !parseInt(hidden_number)) {
            do {
                hidden_number = prompt('Значение не верно! Введите число!', '')
            }
            while (hidden_number.length == '' || !parseInt(hidden_number));
        }

        if (hidden_number > required_number) hidden_number = prompt("Искомое число меньше!");

        else if (hidden_number < required_number) hidden_number = prompt("Искомое число больше!");

        else {
            break;
             }
    }
    if (confirm("Вы угадали! Количество попыток: " + count + ". Начать заново?")) gess();
    else window.close();
}
