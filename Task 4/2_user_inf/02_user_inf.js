name = prompt('Пожалуйста, введите Ваше имя','Вася');

if (name == '') {
    do {
        name = prompt('Вы ничего не ввели! \nПожалуйста, введите Ваше имя', '')
    }
    while (name.length == 0);
}
name = name[0].toUpperCase() + name.slice(1);

age = prompt('Сколько Вам лет?', '');

   if (age.length == 0 || age <= 0 || !parseInt(age)) {
       do {
           age = prompt('Введите корректный возраст', '')
       }
       while (age.length == 0 || age <= 0 || !parseInt(age));
   }
   let str;
if (age == 1 || age % 10 === 1) {str = 'год'}
else if (age > 1 && age < 5 || age % 10 > 1 && age % 10 < 5) {str = 'года'}
else if (age > 4 || age % 10 > 4) {str = 'лет'}



alert('Привет, ' + name + ', тебе уже ' + age + ' ' + str);