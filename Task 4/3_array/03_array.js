let arr = [];

function randInt(min, max){
    let rand = Math.random() * (max - min);
    return Math.round(rand);
}
for (let i = 0, n = 5; i < n; i++) {
    arr.push(randInt(0, 20))
}

function sortirovka(arr, sortm){

    switch (sortm) {
        case 'asc':
            for (let i = 0; i < arr.length - 1; i++) {
                for (let j = 0; j < arr.length - 1 - i; j++) {
                    if (arr[j] < arr[j + 1]) {
                        let swap = arr[j];
                        arr[j] = arr[j + 1];
                        arr[j + 1] = swap;
                    }
                }
            }
            return arr;
            //return arr.sort((a, b) => b - a);
            break;
        case 'desc':
            for (let i = 0; i < arr.length - 1; i++) {
                for (let j = 0; j < arr.length - 1 - i; j++) {
                    if (arr[j] > arr[j + 1]) {
                        let swap = arr[j];
                        arr[j] = arr[j + 1];
                        arr[j + 1] = swap;
                    }
                }
            }
            return arr;
           // return arr.sort((a, b) => a - b);
            break;
    }
}


function sumSq(arr){
    let sum = 0;
for (let i = 0; i < arr.length; i++){
    if (arr[i] % 2 !== 0){
        sum += Math.pow(arr[i], 2);
    }
}
    return sum;
}

function sumSqOneLine1(){
    return arr.filter(function (number){ return number % 2 !== 0}).reduce(function (sum, current) {return sum + Math.pow(current, 2)}, 0);
}
function sumSqOneLine2() {
    return arr.filter(number => number % 2 !== 0).reduce((sum, current) => sum + current ** 2, 0);
} //так лучше!!!
alert('Массив случайных значений: ' + arr);
alert('Сортировна по убыванию: ' + sortirovka(arr, 'asc'));
alert('Сортировна по возрастанию: ' + sortirovka(arr, 'desc'));
alert('Сумма квадратов всех нечетных чисел: ' + sumSq(arr));
alert('Сумма квадратов всех нечетных чисел (код почти в одну строку): ' + sumSqOneLine1());
alert('Сумма квадратов всех нечетных чисел (код реально в одну строку): ' + sumSqOneLine2());

