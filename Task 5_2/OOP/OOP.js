class Group {
    constructor(groupName, genre) {
        this.groupName = groupName;
        this.genre = genre;
    }

    get groupInf() {
        console.log(`${this.groupName} - одна из лучших групп в жанре ${this.genre}`);
        console.log('')
    }
}

let daiteTank = new Group('Дайте Танк(!)', 'гаражный рок');
daiteTank.groupInf;


class Artist {
    constructor(firstName, lastName, birthday, instrument, group) {
        this.group = group;
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthday = birthday;
        this.instrument = instrument;
    }

    get artistInf() {
       return console.log(`${this.firstName} ${this.lastName} - участник группы ${this.group.groupName}.\nДата рождения ${this.birthday} году.\nИнструмент - ${this.instrument}.`);
    }

     isMozzhuhin() {
        if (this.lastName === 'Мозжухин') {
            console.log('\nПотеряли солиста:(');
            this.instrument = 'вокал';
        }
    }

    set artistName(value){
        [this.firstName, this.lastName] = value.split(" ");
    }
}

let dima = new Artist(' ', ' ', '1990', 'гитара', daiteTank);
dima.artistInf;
let maxim = new Artist('Максим', 'Кульша', '1992', 'гитара', daiteTank);
let serega = new Artist('Сергей', 'Акимов', '1990', 'бас-гитара', daiteTank);
let alex = new Artist('Александр', 'Тимофеев', '1991', 'саксафон', daiteTank);
dima.artistName = 'Дмитрий Мозжухин';
dima.artistInf;
maxim.artistInf;
serega.artistInf;
alex.artistInf;
dima.isMozzhuhin();
maxim.isMozzhuhin();
serega.isMozzhuhin();
alex.isMozzhuhin();
dima.artistInf;
maxim.artistInf;
serega.artistInf;
alex.artistInf;

class Albums {
    constructor(numSong, groupName, artist) {
        this.albumName = ['См. рис. 1', ' См. рис. 2', ' Человеко-часы '];
        this.numSong = ['2', ' 4', ' 12'];
    }

    get infAlbums() {
        console.log(`Альбомы группы: ${this.albumName}, в которых ${this.numSong} песен соответственно.`);
    }

    set albumNameNew(value) {
        console.log('О, вышел новый альбом');
        this.albumName.push(value);
    }

    set numSongsNew(value) {
        this.numSong.push(value);
    }

}

let album = new Albums('Взрослые все забыли', '5');
album.infAlbums;
album.albumNameNew = 'Взрослые все забыли';
album.numSongsNew = ' 7';
album.infAlbums;


class Song {
    constructor(songName, duration) {
        this.songName = songName;
        this.duration = duration;
    }

    get songsInf() {
        return `${this.songName} рейтинг: ${this.duration}`;
    }

    get removeSong() {
        if (this.songName === 'Gkj[fz gtcyz') {
            delete this.songName;
            this.duration = 0;
            return console.log('Песня "Gkj[fz gtcyz" была удалена администрацией!');
        }
    }
}

let song1 = new Song('Маленький', '3:45');
let song2 = new Song('Люди', '4:23');
let song3 = new Song('Gkj[fz gtcyz','2:12');
song1.songsInf;
song2.songsInf;
song3.songsInf;
song1.removeSong;
song2.removeSong;
song3.removeSong;
song1.songsInf;
song2.songsInf;
song3.songsInf
let song4 = new Song('Профессионал', '3:33')

class SongWithRating extends Song {

    constructor(songName, duration, rating) {
        super(songName, duration);
        this.rating = rating;
    }

    get info() {
        let baseInfo = super.songsInf; // вызовет get info родительского класса
        console.log(baseInfo);
        return `${baseInfo} rating: ${this.rating}`;

    }
}
let songRating = new SongWithRating(song1.songName, song1.duration,'1');
songRating.info;