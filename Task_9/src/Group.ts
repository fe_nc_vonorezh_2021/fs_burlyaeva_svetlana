interface IGroup {
    groupName: string,
     genre: string,
    groupInf(): void,
}

export class Group implements IGroup{
        groupName: string;
        genre: string;
        constructor(name: string,gGenre:string ) {
            this.groupName = name;
            this.genre = gGenre;
        }

    groupInf() :void {
        console.log(this.groupName, 'одна из лучших групп в жанре', this.genre);
        console.log('')
    }
}