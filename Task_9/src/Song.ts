interface ISong{
    songName: string,
    duration: string,
    songInf(): void,
    removeSong():void,
}

export class Song implements ISong{
    constructor(
        public songName: string,
        public duration: string,
    ) {
    }

    get songsInf() {
        return `Музыкальная композиция '${this.songName} продолжительностью ${this.duration}`;
    }

    songInf() {
        console.log('Музыкальная композиция ',this.songName, 'продолжительностью', this.duration)
    }

    removeSong() {
        if (this.songName === 'Gkj[fz gtcyz') {
           this.songName = "удалено";
            this.duration = '0';
            return console.log('Песня "Gkj[fz gtcyz" была удалена администрацией!');
        }
    }
}