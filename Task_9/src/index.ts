import { Group } from "./Group";
import { Albums } from "./Albums";
import { Artist } from "./Artist";
import { Song } from "./Song";
import { SongWithRating } from "./SongWithRating";
import { Rider } from "./Rider";

let daiteTank = new Group('Дайте Танк(!)', 'гаражный рок!');
daiteTank.groupInf();


let dima = new Artist(' ', ' ', '1990', 'гитара', daiteTank);
dima.artistInf();
let maxim = new Artist('Максим', 'Кульша', '1992', 'гитара', daiteTank);
let serega = new Artist('Сергей', 'Акимов', '1990', 'бас-гитара', daiteTank);
let alex = new Artist('Александр', 'Тимофеев', '1991', 'саксафон', daiteTank);
dima.artistName = 'Дмитрий Мозжухин';
dima.isMozzhuhin();
maxim.isMozzhuhin();
serega.isMozzhuhin();
alex.isMozzhuhin();
dima.artistInf();
maxim.artistInf();
serega.artistInf();
alex.artistInf();


let album = new Albums;
album.infAlbums();

album.albumNameNew = 'Взрослые все забыли';
album.numSongsNew = 3;
album.infAlbums();

let song1 = new Song('Маленький', '3:45');
let song2 = new Song('Люди', '4:23');
let song3 = new Song('Gkj[fz gtcyz', '2:12');
song1.songInf();
song2.songInf();
song3.songInf();
song1.removeSong();
song2.removeSong();
song3.removeSong();
song1.songInf();
song2.songInf();
song3.songInf();

let songRating = new SongWithRating(song1.songName, song1.duration, '1');
songRating.info();

let rider1 = new Rider(666 , daiteTank);
rider1.raiderInf();
let sbpch = new Group('Самое большое простое число', 'инди рок');
//sbpch.groupInf();
let rider2 = new  Rider('5 банок газировки и калькулятор', sbpch);
rider2.raiderInf();