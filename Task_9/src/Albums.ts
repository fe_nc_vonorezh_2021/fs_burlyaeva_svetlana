interface IAlbums{
    albumName: string,
    numSong: number,
    infAlbums():void,
}

export class Albums implements IAlbums{
    albumName: string = 'Человеко-часы';
    numSong: number = 12;
    // constructor(name:string, num:number) {
    //     this.albumName = new name;
    //     this.numSong = new num;
   // }

    infAlbums():void {
        console.log(`\nАльбомы группы: ${this.albumName}, в котором ${this.numSong} песен.`);
    }

    set albumNameNew(value: string) {
        console.log('О, вышел новый альбом');
        this.albumName = value;
    }

    set numSongsNew(value: number) {
        this.numSong = value;
    }
}
