import { Group } from "./Group";

interface IRider <T>{
    things: T,
    group:Group,
}
export class Rider <T> implements IRider<T>{

    constructor(
       public things:T,
       public group: Group,
       ) {}

    raiderInf(){
        console.log( `Артисты группы ${this.group.groupName} написали в райдере ${this.things}`)
    }
}