import { Group } from "./Group";

interface IArtist{
    firstName: string,
    lastName: string,
    birthday: string,
    instrument: string,
    group: Group,
    artistInf(): void,
    isMozzhuhin(): void,

}

export class Artist implements IArtist{

    constructor(
        public firstName: string,
        public lastName: string,
        public birthday: string,
        public instrument: string,
        public group: Group,
    ) {
    }

    artistInf() {
        console.log(this.firstName, this.lastName, '- участник группы ', this.group.groupName, '.\nДата рождения ', this.birthday, ' год.\nИнструмент - ', this.instrument, '.');
    }

    isMozzhuhin() {
        if (this.lastName === 'Мозжухин') {
            console.log(this.firstName, '. Он, кстати, стал солистом группы\n');
            this.instrument = 'вокал';
        }
    }

    set artistName(value: string) {
        console.log("Как же его зовут?");
        [this.firstName, this.lastName] = value.split(" ");
        console.log('А, вспомнил...');

    }
}