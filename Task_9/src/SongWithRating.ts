import { Song } from "./Song";

interface ISongWithRating{
    songName: string,
    duration: string,
    rating: string,
    info(): void,
}

export class SongWithRating extends Song implements ISongWithRating {

    constructor(public songName: string,
                public duration: string,
                public rating: string,) {
        super(songName, duration);
    }

    info() {
        let baseInfo = super.songsInf; // вызовет get info родительского класса
        console.log(baseInfo, "№", this.rating, ' в рейтинге и в моем сердечке');

    }
}