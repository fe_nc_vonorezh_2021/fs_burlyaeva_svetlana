"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SongWithRating = void 0;
const Song_1 = require("./Song");
class SongWithRating extends Song_1.Song {
    constructor(songName, duration, rating) {
        super(songName, duration);
        this.songName = songName;
        this.duration = duration;
        this.rating = rating;
    }
    info() {
        let baseInfo = super.songsInf; // вызовет get info родительского класса
        console.log(baseInfo, "№", this.rating, ' в рейтинге и в моем сердечке');
    }
}
exports.SongWithRating = SongWithRating;
//# sourceMappingURL=SongWithRating.js.map