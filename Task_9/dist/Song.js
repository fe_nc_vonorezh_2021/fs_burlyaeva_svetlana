"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Song = void 0;
class Song {
    constructor(songName, duration) {
        this.songName = songName;
        this.duration = duration;
    }
    get songsInf() {
        return `Музыкальная композиция '${this.songName} продолжительностью ${this.duration}`;
    }
    songInf() {
        console.log('Музыкальная композиция ', this.songName, 'продолжительностью', this.duration);
    }
    removeSong() {
        if (this.songName === 'Gkj[fz gtcyz') {
            this.songName = "удалено";
            this.duration = '0';
            return console.log('Песня "Gkj[fz gtcyz" была удалена администрацией!');
        }
    }
}
exports.Song = Song;
//# sourceMappingURL=Song.js.map