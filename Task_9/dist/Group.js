"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Group = void 0;
class Group {
    constructor(name, gGenre) {
        this.groupName = name;
        this.genre = gGenre;
    }
    groupInf() {
        console.log(this.groupName, 'одна из лучших групп в жанре', this.genre);
        console.log('');
    }
}
exports.Group = Group;
//# sourceMappingURL=Group.js.map