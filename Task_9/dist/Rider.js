"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Rider = void 0;
class Rider {
    constructor(things, group) {
        this.things = things;
        this.group = group;
    }
    raiderInf() {
        console.log(`Артисты группы ${this.group.groupName} написали в райдере ${this.things}`);
    }
}
exports.Rider = Rider;
//# sourceMappingURL=Rider.js.map