"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Artist = void 0;
class Artist {
    constructor(firstName, lastName, birthday, instrument, group) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthday = birthday;
        this.instrument = instrument;
        this.group = group;
    }
    artistInf() {
        console.log(this.firstName, this.lastName, '- участник группы ', this.group.groupName, '.\nДата рождения ', this.birthday, ' год.\nИнструмент - ', this.instrument, '.');
    }
    isMozzhuhin() {
        if (this.lastName === 'Мозжухин') {
            console.log(this.firstName, '. Он, кстати, стал солистом группы\n');
            this.instrument = 'вокал';
        }
    }
    set artistName(value) {
        console.log("Как же его зовут?");
        [this.firstName, this.lastName] = value.split(" ");
        console.log('А, вспомнил...');
    }
}
exports.Artist = Artist;
//# sourceMappingURL=Artist.js.map